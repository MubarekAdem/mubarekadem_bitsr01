$(document).ready(function(){
    $('a[href^="#"]').on('click', function(e){
        e.preventDefault();

        var target = $(this.hash);
        if(target.length){
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 500); 
        }
    });
});
$(document).ready(function () {
    
    function toggleMenu() {
        if ($(window).width() <= 768) {
            $('.navbar').slideUp('slow');
        } else {
            $('.navbar').slideDown('slow');
        }
    }

   
    toggleMenu();

    
    $(window).resize(function () {
        toggleMenu();
    });

    
    $('.menu-btn').click(function () {
        $('.navbar').slideToggle('slow');
    });
});
